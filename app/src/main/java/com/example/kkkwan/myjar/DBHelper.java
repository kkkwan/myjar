package com.example.kkkwan.myjar;

/**
 * Created by Kelly on 3/31/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "myJarsDB";

    private static final String TABLE_HISTORY = "jar_history";
    private static final String HISTORY_KEY_ID = "id";
    private static final String HISTORY_KEY_AMOUNT = "amount";
    private static final String HISTORY_KEY_DATE = "date";
    private static final String HISTORY_KEY_JAR_NUM = "jar_num";
    private static final String HISTORY_KEY_JAR_NAME = "name";

    private static final String TABLE_SUMMARY = "jar_summary";
    private static final String SUMMARY_KEY_ID = "id"; //should match HISTORY_KEY_JAR_NUM
    private static final String SUMMARY_KEY_NAME = "name";
    private static final String SUMMARY_KEY_TOTAL = "total";
    private static final String SUMMARY_KEY_START_DATE = "start_date";
    private static final String SUMMARY_KEY_END_DATE = "end_date";

    private Context mContext;
    private SQLiteDatabase mDb;

    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//        this.mContext = context;
//        this.mDb = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String CREATE_HISTORY_TABLE = "CREATE TABLE " + TABLE_HISTORY + "("
                + HISTORY_KEY_ID + " INTEGER PRIMARY KEY, "
                + HISTORY_KEY_AMOUNT + " REAL,"
                + HISTORY_KEY_DATE + " TEXT," //YYYY-MM-DD HH:MM:SS
                + HISTORY_KEY_JAR_NUM + " INTEGER,"
                + HISTORY_KEY_JAR_NAME + " TEXT" + ")";
        db.execSQL(CREATE_HISTORY_TABLE);

        String CREATE_JARS_TABLE = "CREATE TABLE " + TABLE_SUMMARY + "("
                + SUMMARY_KEY_ID + " INTEGER PRIMARY KEY, "
                + SUMMARY_KEY_NAME + " TEXT,"
                + SUMMARY_KEY_TOTAL + " REAL,"
                + SUMMARY_KEY_START_DATE + " TEXT,"
                + SUMMARY_KEY_END_DATE + " TEXT" + ")";
        db.execSQL(CREATE_JARS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUMMARY);
        onCreate(db);
    }

    //insert entry (how much, when, which jar)
    public void logItem(LogEntry entry){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(HISTORY_KEY_AMOUNT, entry.getAmount());
        values.put(HISTORY_KEY_DATE, entry.getDate());
        values.put(HISTORY_KEY_JAR_NUM, entry.getJarNum());
        values.put(HISTORY_KEY_JAR_NAME, entry.getJarName());
        db.insert(TABLE_HISTORY, null, values);
        db.close();
    }

    //get all entries
    public List<LogEntry> getAllHistory(){
//        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();
        List<LogEntry> entryList = new ArrayList<LogEntry>();
        String selectQuery = "SELECT * FROM " + TABLE_HISTORY + " ORDER BY datetime(" + HISTORY_KEY_DATE + ") DESC";
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                LogEntry entry = new LogEntry();
                entry.setID((Integer.parseInt(cursor.getString(0))));
                entry.setAmount(Double.parseDouble(cursor.getString(1)));
                entry.setDate(cursor.getString(2));
                entry.setJarNum((Integer.parseInt(cursor.getString(3))));
                entry.setJarName(cursor.getString(4));
                entryList.add(entry);
            } while(cursor.moveToNext());
        }

        return entryList;
    }

    //get all summaries of jars
    public List<JarSummary> getAllSummary(){
//        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();
        List<JarSummary> entryList = new ArrayList<JarSummary>();
//        String selectQuery = "SELECT * FROM " + TABLE_SUMMARY + " ORDER BY " + SUMMARY_KEY_NAME;
        String selectQuery = "SELECT * FROM " + TABLE_SUMMARY + " ORDER BY datetime(" + SUMMARY_KEY_END_DATE + ") DESC";
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                JarSummary entry = new JarSummary();
                entry.setID((Integer.parseInt(cursor.getString(0))));
                entry.setName(cursor.getString(1));
                entry.setTotal(Double.parseDouble(cursor.getString(2)));
                entry.setStartDate(cursor.getString(3));
                entry.setEndDate(cursor.getString(4));
                entryList.add(entry);
            } while(cursor.moveToNext());
        }

        return entryList;
    }

    //delete single entry
    public void deleteEntry(LogEntry entry){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_HISTORY, HISTORY_KEY_ID + " = ?", new String[]{String.valueOf(entry.getID())});
        db.close();
    }

    //save jar into summary
    public void saveSummary(JarSummary jar){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SUMMARY_KEY_ID, jar.getID());
        values.put(SUMMARY_KEY_NAME, jar.getName());
        values.put(SUMMARY_KEY_TOTAL, jar.getTotal());
        values.put(SUMMARY_KEY_START_DATE, jar.getStartDate());
        values.put(SUMMARY_KEY_END_DATE, jar.getEndDate());
        db.insert(TABLE_SUMMARY, null, values);
        db.close();
    }

    //update a jar in the summary by checking history table
    public void updateJarSummary(int jarNumber){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues sValues = new ContentValues();

        //if the jar exists, update the information of jar in the summary
        if(getStartDate(jarNumber) != null && getEndDate(jarNumber) != null) {
            sValues.put(SUMMARY_KEY_TOTAL, getTotalOfJar(jarNumber));
            sValues.put(SUMMARY_KEY_START_DATE, getStartDate(jarNumber));
            sValues.put(SUMMARY_KEY_END_DATE, getEndDate(jarNumber));
            db.update(TABLE_SUMMARY, sValues, SUMMARY_KEY_ID + " = ?", new String[]{String.valueOf(jarNumber)});
        }
        else { //if for some reason the jar is invalid, remove that jar entirely
            db.delete(TABLE_SUMMARY, SUMMARY_KEY_ID + " = ?", new String[]{String.valueOf(jarNumber)});
        }

        db.close();
    }

    //update jar summary with a name, such as when renaming a jar
    public void updateJarSummary(int jarNumber, String jarName){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues sValues = new ContentValues();
        ContentValues hValues = new ContentValues();
//        values.put(SUMMARY_KEY_ID, jar.getID());
//        values.put(SUMMARY_KEY_NAME, jar.getName());

        //if the jar exists, update the jar name in the summary
        //also update all the entries for that jar in the history with the new name
        if(getStartDate(jarNumber) != null && getEndDate(jarNumber) != null) {
            sValues.put(SUMMARY_KEY_NAME, jarName);
            sValues.put(SUMMARY_KEY_TOTAL, getTotalOfJar(jarNumber));
            sValues.put(SUMMARY_KEY_START_DATE, getStartDate(jarNumber));
            sValues.put(SUMMARY_KEY_END_DATE, getEndDate(jarNumber));
            db.update(TABLE_SUMMARY, sValues, SUMMARY_KEY_ID + " = ?", new String[]{String.valueOf(jarNumber)});

            hValues.put(HISTORY_KEY_JAR_NAME, jarName);
            db.update(TABLE_HISTORY, hValues, HISTORY_KEY_JAR_NUM + " = ?", new String[]{String.valueOf(jarNumber)});
        }
        else { //if for some reason the jar is invalid, remove that jar entirely
            db.delete(TABLE_SUMMARY, SUMMARY_KEY_ID + " = ?", new String[]{String.valueOf(jarNumber)});
        }

        db.close();
    }

    //get total of jar from the history table
    public double getTotalOfJar(int jarNum){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT SUM(" + HISTORY_KEY_AMOUNT + ") FROM jar_history WHERE jar_num = ?";
        Cursor cursor = db.rawQuery(query, new String[]{Integer.toString(jarNum)});

        if(cursor.moveToFirst()){
            return cursor.getDouble(0);
        }
        return 0;
    }

    //get the date of the earliest entry of the given jar
    public String getStartDate(int jarNum){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT MIN(" + HISTORY_KEY_DATE + ") FROM jar_history WHERE jar_num = ?";
        Cursor cursor = db.rawQuery(query, new String[]{Integer.toString(jarNum)});

        cursor.moveToFirst();
        return cursor.getString(0);
    }

    //get the date of the latest entry of the given jar
    public String getEndDate(int jarNum){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT MAX(" + HISTORY_KEY_DATE + ") FROM jar_history WHERE jar_num = ?";
        Cursor cursor = db.rawQuery(query, new String[]{Integer.toString(jarNum)});

        cursor.moveToFirst();
        return cursor.getString(0);
    }

//    public Cursor getAllHistoryData(){
//        Cursor cursor = mDb.rawQuery("SELECT * FROM jar_history", null);
//        cursor.moveToFirst();
//        return cursor;
//    }

    //get the number of the jar given the name
    public int getJarNum(String jarName){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + HISTORY_KEY_JAR_NUM + " FROM jar_history WHERE name = ? LIMIT 1";
        Cursor cursor = db.rawQuery(query, new String[]{jarName});

        if(cursor.moveToFirst()){
            return cursor.getInt(0);
        }
        else return -1;
    }

    //get the name of the jar given the number
    public String getJarName(int jarNum){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + HISTORY_KEY_JAR_NAME + " FROM jar_history WHERE jar_num = ? LIMIT 1";
        Cursor cursor = db.rawQuery(query, new String[]{Integer.toString(jarNum)});

        cursor.moveToFirst();
        return cursor.getString(0);
    }

    //get the number of the newest jar added
    public int getNewestJar(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT MAX(" + SUMMARY_KEY_ID + ") FROM jar_summary";
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            return cursor.getInt(0);
        }
        else return -1;
    }

    //if the jar exists (by checking the summary of jars), return it's number
    //if the jar does not exist, return -1
    public int findJar(String jarName){
        jarName = jarName.toUpperCase();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT " + SUMMARY_KEY_ID + " FROM jar_summary WHERE REPLACE(UPPER(name),' ','') = ?";
        Cursor cursor = db.rawQuery(query, new String[]{jarName.replace(" ", "")});

        if(cursor.moveToFirst()){
            return cursor.getInt(0);
        }
        else return -1;
    }

    //checks history for latest entry and returns the number of that jar
    public int getLastEntryJarNum(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT jar_num FROM jar_history WHERE id = (SELECT MAX(" + HISTORY_KEY_ID + ") FROM jar_history);";
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()){
            return cursor.getInt(0);
        }
        else return -1;
    }
}

