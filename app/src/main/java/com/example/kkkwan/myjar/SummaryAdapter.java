package com.example.kkkwan.myjar;

/**
 * Created by Kelly on 4/2/2017.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class SummaryAdapter extends ArrayAdapter<JarSummary> {
    private Context mContext;
    private DBHelper db = new DBHelper(getContext());


    public SummaryAdapter(Context context, List<JarSummary> entries) {
        super(context, 0, entries);
        mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        String formatStartDate = "";
        String formatEndDate = "";

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.summary_item, parent, false);
        }

        TextView amountText = (TextView) convertView.findViewById(R.id.amount);
        TextView nameText = (TextView) convertView.findViewById(R.id.summary_jar_name);
        TextView dateRangeText = (TextView) convertView.findViewById(R.id.date_range);

        // Get the data item for this position
        final JarSummary jar = getItem(position);

        final String oldJarName = jar.getName();

        //formatting and update UI to show summary entry
        amountText.setText(String.valueOf(jar.getTotal()));
        nameText.setText(jar.getName());
        formatStartDate = jar.getStartDate().substring(5,7) + "-" + jar.getStartDate().substring(8,10) + "-" + jar.getStartDate().substring(0,4);
        formatEndDate = jar.getEndDate().substring(5,7) + "-" + jar.getEndDate().substring(8,10) + "-" + jar.getEndDate().substring(0,4);
        dateRangeText.setText("FROM " + formatStartDate + " TO " + formatEndDate);

        //colors for negative or positive amount
        if(jar.getTotal() < 0) {
            amountText.setTextColor(ContextCompat.getColor(getContext(), R.color.negRed));
        }
        else if (jar.getTotal() == 0){
            amountText.setTextColor(Color.parseColor("#FFFFFF"));
        }
        else {
            amountText.setTextColor(ContextCompat.getColor(getContext(), R.color.posGreen));
        }

        //move negative sign to front of dollar sign if applicable
        String result = String.format("%.2f", jar.getTotal());
        if(result.subSequence(0,1).equals("-")){
            result = "-$" + result.substring(1);
        } else {
            result = "$" + result;
        }
        amountText.setText(result);

        //allow renaming jar by clicking name in summary list
        nameText.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
            final EditText editJarName = new EditText(getContext());

            new android.support.v7.app.AlertDialog.Builder(getContext())
                    .setTitle("Edit Jar Name")
                .setMessage("What do you want to rename the Jar?")
                .setView(editJarName)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String newJarName = "";
                        newJarName = editJarName.getText().toString().toUpperCase().trim();
                        //only do stuff if the new name is valid (aka not blank or too long)
                        if(newJarName.replace(" ", "").length() > 0 && newJarName.replace(" ", "").length() <= 20 && !newJarName.contains("\n")) {
                            //if the jar doesn't already exist, rename the jar and update UI
                            if (findJar(newJarName) == -1) {
                                renameJar(oldJarName, newJarName);
                                refreshJar();
                            }
                            else{ //if we already find a jar with that name
                                Toast.makeText(getContext(), "Jar name already exists!", Toast.LENGTH_LONG).show();
                            }
                        }
                        else{
                            //error messages for invalid jar names
                            if(newJarName.contains("\n")){
                                Toast.makeText(getContext(), "Jar name must not span multiple lines!", Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(getContext(), "Jar name must be at most 20 characters!", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
            }
        });

        // Return the completed view to render on screen
        return convertView;
    }

    //rename the jar by getting jar number and passing the new name
    public void renameJar(String oldName, String newName){
        updateJar(getJarNum(oldName), newName);
    }

    //update the jar with the new name
    public void updateJar(int jarNumber, String jarName){
//        db = new DBHelper(getContext());
        db.updateJarSummary(jarNumber, jarName);
    }

    //get jar number given jar name (search through history table)
    public int getJarNum(String jarName){
//        db = new DBHelper(getContext());
        return db.getJarNum(jarName);
    }

    //get jar number given jar name (search through summary table)
    public int findJar(String jarName){
//        db = new DBHelper(getContext());
        return db.findJar(jarName);
    }

    //update entire UI (history activity, not just the summary list)
    public void refreshJar(){
        if(mContext instanceof History){
            ((History)mContext).refreshView();
        }
    }
}
