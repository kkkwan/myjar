package com.example.kkkwan.myjar;

/**
 * Created by Kelly on 3/31/2017.
 */

public class JarSummary {
    private int id;
    private String name;
    private double total;
    private String startDate;
    private String endDate;
    private int jarNum;

    public JarSummary(){

    }

    public JarSummary(int id, String name, double total, String startDate, String endDate){
        this.id = id;
        this.name = name;
        this.total = total;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public void setID(int id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setTotal(double total){
        this.total = total;
    }

    public void setStartDate(String startDate){
        this.startDate = startDate;
    }

    public void setEndDate(String endDate){ this.endDate = endDate; }

    public int getID(){
        return id;
    }

    public String getName(){
        return name;
    }

    public double getTotal(){
        return total;
    }

    public String getStartDate(){
        return startDate;
    }

    public String getEndDate(){
        return endDate;
    }

}
