package com.example.kkkwan.myjar;

/**
 * Created by Kelly on 4/2/2017.
 */

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class History extends AppCompatActivity {

    private DBHelper db;
    private SimpleCursorAdapter sca;
    private ListView historyListView;
    private ListView summaryListView;
    private List<LogEntry> historyList = new ArrayList<>();
    private List<JarSummary> summaryList = new ArrayList<>();;
    private HistoryAdapter hAdapter;
    private SummaryAdapter sAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //history activity

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        historyListView = (ListView) findViewById(R.id.history_list);
        historyList = getHistory();
//        Collections.reverse(historyList);
        hAdapter = new HistoryAdapter(this, historyList);
        historyListView.setAdapter(hAdapter);

        summaryListView = (ListView) findViewById(R.id.summary_list);
        summaryList = getSummary();
//        Collections.reverse(summaryList);
        sAdapter = new SummaryAdapter(this, summaryList);
        summaryListView.setAdapter(sAdapter);

        //testing jar refresh manually
//        TextView jarsText = (TextView) findViewById(R.id.jars_text);
//        jarsText.setOnClickListener(new View.OnClickListener(){
//            public void onClick(View v){
////                TextView jarsText = (TextView) findViewById(R.id.jars_text);
////                jarsText.setText("This should refresh the entries.");
//
//                refreshView();
//
//            }
//        });

    }

    public void refreshView(){
        //history ordered by date
        historyList = getHistory();
//        Collections.reverse(historyList);
        hAdapter = new HistoryAdapter(this, historyList);
        historyListView.setAdapter(hAdapter);

        //summary ordered date of last entry
        summaryList = getSummary();
//        Collections.reverse(summaryList);
        sAdapter = new SummaryAdapter(this, summaryList);
        summaryListView.setAdapter(sAdapter);

        //update the UI in real time
        hAdapter.notifyDataSetChanged();
        sAdapter.notifyDataSetChanged();
    }

    //get entries for history
    public List<LogEntry> getHistory(){
        db = new DBHelper(this);
        return db.getAllHistory();
    }

    //get entries for summary
    public List<JarSummary> getSummary(){
        db = new DBHelper(this);
        return db.getAllSummary();
    }
}
