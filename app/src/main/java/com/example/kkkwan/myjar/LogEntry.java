package com.example.kkkwan.myjar;

/**
 * Created by Kelly on 3/31/2017.
 */

public class LogEntry {
    private int id;
    private double amount;
    private String date;
    private int jarNum;
    private String jarName;

    public LogEntry(){

    }

    public LogEntry(int id, double amount, String date, int jarNum, String jarName){
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.jarNum = jarNum;
        this.jarName = jarName;
    }

    public void setID(int id){
        this.id = id;
    }

    public void setAmount(double amount){
        this.amount = amount;
    }

    public void setDate(String date){
        this.date = date;
    }

    public void setJarNum(int jarNum){
        this.jarNum = jarNum;
    }

    public void setJarName(String jarName){ this.jarName = jarName; }

    public int getID(){
        return id;
    }

    public double getAmount(){
        return amount;
    }

    public String getDate(){
        return date;
    }

    public int getJarNum(){
        return jarNum;
    }

    public String getJarName(){ return jarName; }



}
