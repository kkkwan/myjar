package com.example.kkkwan.myjar;

/**
 * Created by Kelly on 4/2/2017.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class HistoryAdapter extends ArrayAdapter<LogEntry> {

    private DBHelper db = new DBHelper(getContext());
    private Context mContext;

    public HistoryAdapter(Context context, List<LogEntry> entries) {
        super(context, 0, entries);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String formatDate = "";

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.history_item, parent, false);
        }

        TextView amountText = (TextView) convertView.findViewById(R.id.amount);
        TextView jarNameText = (TextView) convertView.findViewById(R.id.history_jar_name);
        TextView dateText = (TextView) convertView.findViewById(R.id.date);
        Button deleteButton = (Button) convertView.findViewById(R.id.delete_button);

        // Get the data item for this position
        final LogEntry entry = getItem(position);

        //formatting and update UI to show history entry
        amountText.setText(String.valueOf(entry.getAmount()));
        jarNameText.setText(entry.getJarName());
        formatDate = entry.getDate().substring(5,7) + "-" + entry.getDate().substring(8,10) + "-" + entry.getDate().substring(0,4);
        formatDate = formatDate + "        " + entry.getDate().substring(11,19);
        dateText.setText(formatDate);

        //colors for negative or positive amount
        if(entry.getAmount() < 0) {
            amountText.setTextColor(ContextCompat.getColor(getContext(), R.color.negRed));
        }
        else if (entry.getAmount() == 0){
            amountText.setTextColor(Color.parseColor("#FFFFFF"));
        }
        else {
            amountText.setTextColor(ContextCompat.getColor(getContext(), R.color.posGreen));
        }

        //move negative sign to front of dollar sign if applicable
        String result = String.format("%.2f", entry.getAmount());
        if(result.subSequence(0,1).equals("-")){
            result = "-$" + result.substring(1);
        } else {
            result = "$" + result;
        }
        amountText.setText(result);

        //button to delete entry
        deleteButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
            new AlertDialog.Builder(getContext())
                .setTitle("Delete Entry")
                .setMessage("Are you sure you want to delete this entry?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    deleteEntry(entry);
                    refreshJar();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
            }
        });

        // Return the completed view to render on screen
        return convertView;
    }

    //delete entry from history table and notify to update UI
    //also update summary of jars
    public void deleteEntry(LogEntry entry){
        db.deleteEntry(entry);
        remove(entry);
        notifyDataSetChanged();

        updateJar(entry.getJarNum(), entry.getJarName());
//        db.close();
    }

    //update summary of jars given jar number and name
    public void updateJar(int jarNumber, String jarName){
        db.updateJarSummary(jarNumber, jarName);
    }

    //update entire UI (history activity, not just the history entries list)
    public void refreshJar(){
        if(mContext instanceof History){
            ((History)mContext).refreshView();
        }
    }
}
