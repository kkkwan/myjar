package com.example.kkkwan.myjar;


/*
Author: Kelly Kwan
March 2017
*/


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.format.DateFormat;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.text.TextUtils.isDigitsOnly;

public class MainActivity extends AppCompatActivity {

    private DBHelper db = new DBHelper(this);
    private Cursor cur;

    private EditText inputAmount;
    private Button addButton;
    private Button subButton;
    private Button resetButton;
    private Button historyButton;
//    private Button newJarButton;
    private Button jarNameButton;
    private DatePicker datePicked;

    private int jarNumber = -1;
    private String jarName = "myJar".toUpperCase();
//    private boolean jarChanged = false;
    private int jarExists = -1;
    private double total = 0;


    @Override
    protected void onResume(){
        super.onResume();

        //if there are no entries, create a first jar
        if(getLastEntryJarNum() == -1){
            createFirstJar(++jarNumber, jarName);
        }

        //if there was at least one entry,
        //get the jar with the latest entry
        if (getLastEntryJarNum() != -1) {
            jarNumber = getLastEntryJarNum();
            jarName = getJarName(jarNumber);
        }

        //update display
        displayJarName(jarNumber);

        total = getTotal(jarNumber);
        displayTotal(total);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //jar name displayed at top acts as a button
        this.jarNameButton = (Button) findViewById(R.id.jar_name);

        //when jar name button (including pencil icon) pressed, show dialog box
        jarNameButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
            //dialog box has text field to change jar name
            final EditText editJarName = new EditText(MainActivity.this);

            new android.support.v7.app.AlertDialog.Builder(MainActivity.this)
            .setTitle("Edit Jar Name")
            .setMessage("Which jar are you adding to?")
            .setView(editJarName)
            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                jarName = editJarName.getText().toString().toUpperCase().trim();
                //only do stuff if the new name is valid (aka not blank or too long)
                if(jarName.replace(" ", "").length() > 0 && jarName.replace(" ", "").length() <= 20 && !jarName.contains("\n")) {
                    MainActivity.this.jarNameButton.setText(jarName);

                    //if the jar already exists, get the existing jar
                    jarExists = findJar(jarName);
                    if (jarExists != -1) {
                        jarNumber = jarExists;
                    }
                    else {
                        //if the jar is new, it will be empty with no entries
                        jarNumber = -1;
                    }

//                        MainActivity.this.jarNameButton.setText(String.valueOf(jarNumber)); //test

                    total = getTotal(jarNumber);
                    displayTotal(total);

                }
                else{
                    //error messages for invalid jar names
                    if(jarName.contains("\n")){
                        Toast.makeText(MainActivity.this, "Jar name must not span multiple lines!", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(MainActivity.this, "Jar name must be at most 20 characters!", Toast.LENGTH_LONG).show();
                    }
                }

                return;

                }
            })
            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                // do nothing
                return;
                }
            })
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show();
            }
        });

        //display jar total
        total = getTotal(jarNumber);
        displayTotal(total);

        //text box for amount to add
        this.inputAmount = (EditText) findViewById(R.id.input_value);

        //addition button
        this.addButton = (Button) findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
            String amountStr = "";

            //if the jar has no entries, assign new jar number, create and save jar
            jarExists = findJar(jarName);
            if (jarExists == -1) {
                jarNumber = newJarNum();
                createJar(jarName, jarNumber);
            }

            //if there is input...
            if(inputAmount.getText().toString().length() > 0){
                try {

                    //truncate input if there are more than two decimal places
                    //$1.009 -> $1.00
                    String input = inputAmount.getText().toString();
                    if(input.contains(".")){
                        int afterDecLen = input.substring(input.indexOf(".")).length();
                        if(afterDecLen > 2){
                            input = input.substring(0, input.length() - afterDecLen + 3); //decimal + 2 digits
                        }
                    }
                    amountStr = String.format("%.2f",Double.parseDouble(input));
//                        setDebugText(amountStr);

                    String date = getDatePicked();

                    //store entry in db
                    insertEntry(Double.parseDouble(amountStr), date, jarNumber, jarName);

                    //display new total
                    double total = getTotal(jarNumber);
                    displayTotal(total);

                    //remind user how much they added and clear input
                    Toast.makeText(MainActivity.this, "$" + amountStr + " has been added!", Toast.LENGTH_LONG).show();
                    inputAmount.setText("");

                    //update total for the summaries here, just to be safe
                    updateJar(jarNumber);

                } catch (NumberFormatException e){ setDebugText(""); }
            } else { setDebugText(""); }

            //hide keyboard
            hideKeyboard();

            return;
            }
        });

        //subtract button, similar to addition button
        this.subButton = (Button) findViewById(R.id.sub_button);
        subButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                String amountStr = "";

                jarExists = findJar(jarName);
                if (jarExists == -1) {
                    jarNumber = newJarNum();
                    createJar(jarName, jarNumber);
                }

                if(inputAmount.getText().toString().length() > 0){
                    try {
                        String input = inputAmount.getText().toString();
                        if(input.contains(".")){
                            int afterDecLen = input.substring(input.indexOf(".")).length();
                            if(afterDecLen > 2){
                                input = input.substring(0, input.length() - afterDecLen + 3); //decimal + 2 digits
                            }
                        }
                        amountStr = String.format("%.2f",Double.parseDouble(input));
//                        setDebugText(amountStr);

                        String date = getDatePicked();

                        insertEntry(0-Double.parseDouble(amountStr), date, jarNumber, jarName);

                        double total = getTotal(jarNumber);
                        displayTotal(total);

                        Toast.makeText(MainActivity.this, "$" + amountStr + " has been subtracted!", Toast.LENGTH_LONG).show();
                        inputAmount.setText("");

                        updateJar(jarNumber);
                    } catch (NumberFormatException e){ setDebugText(""); }
                } else { setDebugText(""); }

                hideKeyboard();

                return;
            }
        });

        //button to reset calendar to current day
        this.resetButton = (Button) findViewById(R.id.reset_date_button);
        resetButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
            Calendar today = Calendar.getInstance();
            int year = today.get(Calendar.YEAR);
            int month = today.get(Calendar.MONTH);
            int day = today.get(Calendar.DAY_OF_MONTH);

            datePicked = (DatePicker) findViewById(R.id.date_picker);
            datePicked.updateDate(year, month, day);

            return;
            }
        });

        //button to see past entries and summary of jars
        this.historyButton = (Button) findViewById(R.id.history_button);
        historyButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
            Intent intent = new Intent(MainActivity.this, History.class); //intent for specific component
            startActivity(intent);

            }
        });
}


    //update UI for testing purposes
    public void setDebugText(String text){
        TextView debugText = (TextView) findViewById(R.id.debug);
        debugText.setText(text);
    }

    //insert entry to db
    public void insertEntry(double amount, String date, int jarNum, String jarName){
        db.logItem(new LogEntry(0, amount, date, jarNum, jarName));
    }

    //get the total of the jar
    public double getTotal(int jarNum){
        return db.getTotalOfJar(jarNum);
    }

    //update UI to display total of current jar
    public void displayTotal(double total){
        TextView currentTotal = (TextView) findViewById(R.id.current_total);

        //change color of text depending on positive or negative total
        if(total < 0) {
            currentTotal.setTextColor(ContextCompat.getColor(this, R.color.negRed));
        }
        else if (total == 0){
            currentTotal.setTextColor(Color.parseColor("#FFFFFF"));
        }
        else {
            currentTotal.setTextColor(ContextCompat.getColor(this, R.color.posGreen));
        }

        //display negatives in front of dollar sign
        String result = String.format("%.2f", total);
        if(result.subSequence(0,1).equals("-")){
            result = "-$" + result.substring(1);
        } else {
            result = "$" + result;
        }
        currentTotal.setText(result);
    }

    //update jar name given jar number in the UI
    public void displayJarName(int jarNumber){
        TextView jarNameButton;
        jarNameButton = (Button) findViewById(R.id.jar_name);
        if(jarNumber == -1){
            jarNameButton.setText(jarName);
        }
        else {
            String jarName = getJarName(jarNumber);
            jarNameButton.setText(jarName);
        }
    }

    //save jar into summary of jars
    public void createJar(String name, int jarNum){
//        db = new DBHelper(this);

        String startDate = db.getStartDate(jarNum);
        String endDate = db.getEndDate(jarNum);

        if(startDate == null || endDate == null){
            //make start and end date the same
            startDate = getDatePicked();
            endDate = getDatePicked();
        }

        db.saveSummary(new JarSummary(jarNum, name, getTotal(jarNum), startDate, endDate));
    }

    //get the jar number given the name
    public int getJarNum(String jarName){
        return db.getJarNum(jarName);
    }

    //get the jar name given the number
    public String getJarName(int jarNum){
        return db.getJarName(jarNum);
    }

    //create new jar by incrementing latest jar number
    public int newJarNum(){
        int lastJar = db.getNewestJar();
        return ++lastJar;
    }

    //check if jar exists
    public int findJar(String jarName){
        return db.findJar(jarName);
    }

    //get the number of the jar for the last entry added
    public int getLastEntryJarNum(){
        return db.getLastEntryJarNum();
    }

    //update the jar summary given the jar number
    public void updateJar(int jarNumber){
        db.updateJarSummary(jarNumber);
    }

    //update the jar summary given the jar number and name
    public void updateJar(int jarNumber, String jarName){
        db.updateJarSummary(jarNumber, jarName);
    }

    //get the date in format: YYYY-MM-DD HH:MM:SS
    public String getDatePicked(){
        datePicked = (DatePicker) findViewById(R.id.date_picker);
        int day = datePicked.getDayOfMonth();
        int month = datePicked.getMonth() + 1;
        int year = datePicked.getYear();

        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int min = c.get(Calendar.MINUTE);
        int sec = c.get(Calendar.SECOND);

        String date = String.format("%04d-%02d-%02d %02d:%02d:%02d", year, month, day, hour, min, sec);
        return date;
    }

    //create the very first jar to add to db
    public void createFirstJar(int jarNumber, String jarName){
        createJar(jarName, jarNumber);
        String date = getDatePicked();
        insertEntry(0, date, jarNumber, jarName);
    }

    //hide the keyboard
    public void hideKeyboard(){
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

}
