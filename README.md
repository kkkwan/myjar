This is an app I made for fun intended to record savings and expenditure.
Think of it as having a bunch of jars with different labels. You put in money every now and then or maybe take some out. You typically don't care when or why you're putting in money. You just have a goal to reach or just want to save up. In the end, you just see jars that hopefully have something in it.

My app lets you record when and how much you have you've "put in each jar" and view them at a glance. It's nice to see how much you've been saving and spending in different categories, and maybe develop better habits.

Some use cases...
Recording how much:
* you've been spending on groceries, food, etc.
* money you owe or are owed
* tips from a job
* change you've been saving up and comparing it to previous years
* you've won or lost in Las Vegas
* you typically pay for a haircut
* etc.

